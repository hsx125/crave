<form class="form-inline mr-auto">
    <ul class="navbar-nav mr-3">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fa fa-bars"></i></a></li>
    </ul>
</form>
<ul class="navbar-nav navbar-right">
    <li class="dropdown dropdown-list-toggle"><a href="#" data-toggle="dropdown" class="nav-link notification-toggle nav-link-lg beep"><i class="fa fa-bell"></i></a>
        <div class="dropdown-menu dropdown-list dropdown-menu-right">
            <div class="dropdown-header">Pemberitahuan
                <div class="float-right">
                    <!-- <a href="#">Mark All As Read</a> -->
                </div>
            </div>
            <div class="dropdown-list-content dropdown-list-icons show_notifikasi">

            </div>
            <!-- <div class="dropdown-footer text-center">
                <a href="#">View All <i class="fa fa-chevron-right"></i></a>
            </div> -->
        </div>
    </li>
    <li class="dropdown">
        <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
            <img alt="" src="{{ show_photo_user(auth()->user()->id) }}" style="width:25px;height:25px" class="rounded-circle mr-1">
            <div class="d-sm-none d-lg-inline-block">Hi, {{ auth()->user()->name }}</div>
        </a>
        <div class="dropdown-menu dropdown-menu-right">
            <a href="" class="dropdown-item has-icon">
                <i class="fa fa-user"></i> Profile
            </a>
            <div class="dropdown-divider"></div>
            <a href="{{ route('logout') }}" class="dropdown-item has-icon text-danger">
                <i class="fa fa-sign-out"></i> Logout
            </a>
        </div>
    </li>
</ul>
