<section class="section">
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-statistic-2">
                <div class="card-stats">
                    <div class="card-stats-title">
                        <div class="title-app">{{ config('app.name') }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
