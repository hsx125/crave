@extends('backend.layout')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-box">{{ $title }}</div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        @if(session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                        @endif
                        <form method="post" action="{{ route('category.update', $category->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                {!! eform_input('text', 'Name', 'name', $category->name) !!}
                                @error('name')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_area('Description', 'description', $category->description) !!}
                                @error('description')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'Price', 'price', number_format($category->price,0,',','.'), 'onkeyup="formatMoney($(this))"') !!}
                                @error('price')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_select('Status', 'status', $status, $category->status) !!}
                                @error('price')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div>
                                <a href="{{ route('category.index') }}" class="btn btn-danger">
                                    Kembali
                                </a>
                                <button type="submit" class="btn btn-primary">
                                    Simpan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
<script>
    $('form').on('submit', function() {
        $('button').attr('disabled', true)
        $('a').addClass('disabled')
    })
</script>
@endpush