@extends('backend.layout')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-10">
                <div class="title-box">{{ $title }}</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="{{ route('category.create') }}" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i>
                        Create New
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">No</th>
                                        <th style="width:15%">Name</th>
                                        <th>Description</th>
                                        <th style="width: 10%;">Price</th>
                                        <th style="width: 10%;">Status</th>
                                        <th style="width: 7%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php $no = 0; @endphp
                                    @forelse ($categories as $category)
                                    @php ++$no; @endphp
                                    <tr>
                                        <td class="text-center">{{ $no }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>{{ $category->description }}</td>
                                        <td class="text-right">{{ number_format($category->price,0,',','.') }}</td>
                                        <td class="text-center">
                                            {!! $category->status == 1 ? '<label class="btn btn-xs btn-outline-success">Publish</label>' : '<label class="btn btn-xs btn-outline-danger">Not Publish</label>' !!}
                                        </td>
                                        <td class="text-center">
                                            <a class="btn btn-xs btn-success" href="{{ route('category.edit', $category->id) }}"><i class="fa fa-edit"></i></a>
                                            <a class="btn btn-xs btn-danger" href="javascript:;" onclick="btnDelete('{{ route('category.destroy', $category->id) }}')"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="6" class="text-center">Tidak ada data untuk ditampilkan.</td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')
<script>
    
    function btnDelete(deleted) {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Anda yakin ingin menghapus data ini?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya. Lanjutkan!',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: `${deleted}`,
                    type: 'post',
                    dataType: 'json',
                    data: { _method: 'DELETE' },
                    success: function(result) {
                        if (result.status == 'success') {
                            show_success(result.response)
                            setTimeout(() => {
                                window.location.reload()
                            }, 1000)
                        } else {
                            show_failed(result.response)
                        }
                    },
                    error: function(result) {
                        show_errors(result.responseJSON)
                    }
                })
            } else {
                return false
            }
        })
    }

</script>
@endpush
