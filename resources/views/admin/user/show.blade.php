@extends('backend.layout')
@push('css')
<link rel="stylesheet" href="{{ asset('assets/plugins/leaflet/leaflet.css') }}">
@endpush
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-box">{{ $title }}</div>
            </div>
            
        </div>

        <form method="post" action="{{ route('update.profil', ['id' => auth()->user()->id]) }}">
            @method('put')
            @csrf
            {!! eform_hidden('id', auth()->user()->id) !!}
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            @if(session('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                            @endif
                            @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif

                            <div class="form-group text-center">
                                <div class=" d-flex justify-content-center align-items-center">
                                    <a href="{{ show_photo_user(auth()->user()->id) }}" class="text-decoration-none" target="_blank" rel="noopener noreferrer">
                                        <div class="shadow image1" style="border-radius:100%;background-image: url('{{ show_photo_user(auth()->user()->id) }}');width:170px;height:170px;background-size:cover;background-position:center"></div>
                                        <div class="mt-2">
                                            @if (auth()->user()->email_verified_at <> null)
                                                <strong class=" text-success"><i class="fa fa-check-circle"></i> Terverifikasi</strong>
                                            @else
                                                <strong class=" text-danger">Belum diverifikasi</strong>
                                            @endif
                                        </div>
                                    </a>
                                </div>
                                <div class="text-center">
                                    <a href="javascript:;" onclick="changePhoto()">Ganti foto</a>
                                    <input type="hidden" name="foto" id="foto">
                                </div>
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'Name', 'name', auth()->user()->name) !!}
                                @error('name')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'NIK', 'nik', auth()->user()->nik, 'maxlength="16" onkeyup="number($(this))" required') !!}
                                @error('nik')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('email', 'Email', 'email', auth()->user()->email) !!}
                                @error('email')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'Phone', 'phone', auth()->user()->phone, 'onkeyup="number($(this))" maxlength="13" required') !!}
                                @error('phone')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'Address', 'address', auth()->user()->address, '', '') !!}
                            </div>

                            <div class="form-group">
                                {!! eform_select_all('Kecamatan', 'kecamatan', $district, auth()->user()->district_id, $disable, '') !!}
                            </div>

                            <div class="form-group">
                                {!! eform_select_all('Desa', 'desa', $village, auth()->user()->village_id, $disable, '') !!}
                            </div>

                            <div class="form-group">
                                {!! eform_input('password', 'Password', 'password', '', '') !!}
                                @error('password')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('password', 'Password Confirmation', 'password_confirmation', '', '') !!}
                                @error('password_confirmation')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">
                                    Simpan Perubahan
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h6 class="mb-0">Lokasi</h6>
                        </div>
                        <div class="card-body" style="padding: 0 !important;overflow:hidden;">
                            <div style="width: 100%;height:50vh;" id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </section>
@endsection

@push('js')
<script src="{{ asset('assets/plugins/leaflet/leaflet.js') }}"></script>
<script src="{{ asset('assets/plugins/webcamjs/webcam.min.js') }}"></script>
<script>
    $('#kecamatan').on('change', function() {

        $('#desa').empty().html(`<option value="0">Semua Desa</option>`)

        $.post(`{{ route('json.village') }}`, { district: $('#kecamatan').val() }).then((response) => {
            var option = ``
            if (response.data.length > 0) {
                option += `<option value="0">Pilih Desa</option>`
                response.data.forEach(function(element, index) {
                    option += `<option value="${element.id}">${element.name}</option>`
                });
            } else {
                option += `<option value="0">Data Desa Kosong</option>`
            }
            $('#desa').html(option)
        }).catch((error) => {
            console.log(error)
        })

    });

    Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    const take_snapshot = () => {

        Webcam.snap(function (data_uri) {

            $('.image1').css({
                'background-image' : `url('${data_uri}')`
            })
            $('#foto').val(data_uri)
        });

        Webcam.reset();
        Modal.close()
    }

    const changePhoto = () => {
        Modal.createNoFooter('Ambil Foto', 'modal-md')
        Modal.html(`
            <div class="text-center mb-3">Tunggu sampai kamera terbuka</div>
            <div class="d-flex justify-content-center align-items-center">
                <div id="my_camera"></div>
            </div>
            <div class="text-center mt-3 take-img d-none"><button class="btn btn-info" type=button onclick="take_snapshot()"><i class="fa fa-camera"></i> Ambil foto</button></div>
        `)
        Webcam.attach('#my_camera');

        navigator.mediaDevices.getUserMedia({
                video: true
            })
            .then(stream => {
                if(stream.active == true) {
                    $('.take-img').removeClass("d-none")
                }
            })
            .then(() => $('.take-img').removeClass("d-none"))
            .catch(error => {
                $('.take-img').addClass("d-none")
                alert(error)
            });

    }

    $('form').on('submit', function () {
        $('button').attr('disabled', true)
    })

    var map = L.map('map').setView([{{ auth()->user()->latitude ?? config('app.default_lat') }}, {{ auth()->user()->longitude ?? config('app.default_long') }}], {{ intval(config('app.default_zoom')) }});

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    L.marker([{{ auth()->user()->latitude ?? config('app.default_lat') }}, {{ auth()->user()->longitude ?? config('app.default_long') }}]).addTo(map)
        .bindPopup('{{ auth()->user()->name }}')
        .openPopup();
</script>
@endpush