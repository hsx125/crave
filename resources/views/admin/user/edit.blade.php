@extends('layout')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-box">{{ $title }}</div>
            </div>
            
        </div>

        <form method="post" action="{{ route('user.update', $user->id) }}">
            @method('PUT')
            @csrf
            {!! eform_hidden('id', $user->id) !!}
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            @if(session('error'))
                            <div class="alert alert-danger">
                                {{ session('error') }}
                            </div>
                            @endif

                            <div class="form-group">
                                {!! eform_input('text', 'Name', 'name', $user->name) !!}
                                @error('name')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('email', 'Email', 'email', $user->email) !!}
                                @error('email')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('password', 'Password', 'password', '', '') !!}
                                @error('password')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                {!! eform_input('password', 'Password Confirmation', 'password_confirmation', '', '') !!}
                                @error('password_confirmation')
                                    <div class="is-invalid">{{ $message }}</div>
                                @enderror
                            </div>

                            <div>
                                <button onclick="window.location.href = '{{ route('user.index') }}'" class="btn btn-danger">
                                    Kembali
                                </button>
                                <button type="submit" class="btn btn-primary">
                                    Simpan
                                </button>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </form>
    </section>
@endsection

@push('js')
<script>
    $('form').on('submit', function() {
        $('button').attr('disabled', true)
    })
    
</script>
@endpush