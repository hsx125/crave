@extends('layout')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-10">
                <div class="title-box">{{ $title }}</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="{{ route('user.create') }}" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i>
                        Create New
                    </a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-lg-12">
                @if(session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th style="width: 7%;">Action</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')
<script>

    const btnDelete = (deleted) => {
        Swal.fire({
            title: 'Confirmation',
            text: "Are you sure delete this data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: `${deleted}`,
                    type: 'post', 
                    data: {
                        _method: 'DELETE',
                    },
                    dataType: 'json',
                    success: function(res) {
                        if (res.status == true) {
                            show_success(res.response)
                            table.ajax.reload()
                        } else {
                            show_failed(res.response)
                        }
                    },
                    error: function(error) {
                        show_errors(error.responseJSON)
                    }
                })
            } else {
                return false
            }
        })
    }

    var table = $('.table').DataTable({
        ordering: false,
        serverSide: true,
        processing: true,
        responsive: true,
        stateSave: true,
        ajax: {
            url: `{{ route('user.list') }}`,
            type: 'post',
        },
        columns: [
            { "data": "no", sClass:"text-center" },
            { "data": "name" },
            { "data": "email" },
            { "data": null, sClass:"text-center", render: function(data, type, row, meta) {
                var url_edit = `{{ route('user.edit', ':id') }}`
                url_edit = url_edit.replace(':id', data.id)

                var url_hapus = `{{ route('user.destroy', ':id') }}`
                url_hapus = url_hapus.replace(':id', data.id)
                return `
                        <a class="btn btn-xs btn-success" href="${url_edit}"><i class="fa fa-edit"></i></a>
                        <a class="btn btn-xs btn-danger" href="javascript:;" onclick="btnDelete('${url_hapus}')"><i class="fa fa-trash"></i></a>
                    `
            } },
        ],
        initComplete: function() {
            var api = this.api();
            $("input[type='search']")
                .off(".DT")
                .on("keyup.DT, blur", function(e) {
                    if (e.keyCode === 13) {
                        api.search(this.value).draw();
                    }
                });
            $("input[type='search']").attr('placeholder', 'Press Enter to search')
        },
    })
</script>
@endpush
