@extends('backend.layout')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <div class="title-box">{{ $title }}</div>
            </div>
            
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        @if($errors->any())
                            {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
                        @endif
                        <form method="post" action="{{ route('setting.store') }}">
                            @csrf

                            <div class="form-group">
                                {!! eform_input('text', 'Company', 'company', setting('company') ?? '') !!}
                            </div>
                            
                            <div class="form-group">
                                {!! eform_input('text', 'Nama Aplikasi', 'app_name', setting('app_name') ?? '') !!}
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'Versi Aplikasi', 'app_version', setting('app_version') ?? '') !!}
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'Email', 'email', setting('email') ?? '') !!}
                            </div>

                            <div class="form-group">
                                {!! eform_input('text', 'Telp/Mobile', 'phone', setting('phone') ?? '') !!}
                            </div>

                            <div class="form-group">
                                {!! eform_area('Alamat', 'address', setting('address') ?? '') !!}
                            </div>

                            <div>
                                <button type="submit" class="btn btn-primary">
                                    Simpan Pengaturan
                                </button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@push('js')
<script>

</script>
@endpush