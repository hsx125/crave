@extends('layout')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-10">
                <div class="title-box">{{ $title }}</div>
            </div>
            <div class="col-lg-2">
                <div class="pull-right">
                    <a href="javascript:;" onclick="btnAdd()" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i>
                        Create New
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-hover mb-0">
                                <thead>
                                    <tr>
                                        <th style="width: 3%;">No</th>
                                        <th style="width: 15%;">Name</th>
                                        <th>Description</th>
                                        <th style="width: 7%;">Subscriptions</th>
                                        <th style="width: 7%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse ($templates as $key => $template)
                                        <tr>
                                            <td class="text-center">{{ $key+1 }}</td>
                                            <td>{{ $template->name }}</td>
                                            <td>{{ $template->description }}</td>
                                            <td>
                                                @isset($template->subscriptions)
                                                    @foreach ($template->subscriptions as $sub)
                                                        {{ $sub->subscription->name }}
                                                    @endforeach
                                                    
                                                @endisset
                                            </td>
                                            <td class="text-center">
                                                <a class="btn btn-xs btn-success" href="javascript:;" onclick="btnEdit('{{ $template->id }}')"><i class="fa fa-edit"></i></a>
                                                <a class="btn btn-xs btn-danger" href="javascript:;" onclick="btnDelete('{{ $template->id }}')"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5" class="text-center">No data available.</td>
                                        </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('js')
<script>
    function btnAdd() {
        Modal.create('Create New Role', 'modal-lg')
        $.get(`{{ route('subscription.create') }}`, function(data) {
            Modal.html(data)

            $(".btn-simpan").on('click', function(e) {
                e.preventDefault()

                var form = $('#form')

                $("button").attr('disabled', true)

                $.ajax({
                    url: `{{ route('subscription.store') }}`,
                    type: 'post',
                    dataType: 'json',
                    data: form.serializeArray(),
                    success: function(result) {
                        if (result.status == true) {
                            show_success(result.response)
                            setTimeout(() => {
                                window.location.reload()
                            }, 1000)
                        } else {
                            show_failed(result.response)
                            $("button").removeAttr('disabled')
                        }
                    },
                    error: function(result) {
                        $("button").removeAttr('disabled')
                        show_errors(result.responseJSON)
                    }
                })
            })
        })
    }

    function btnEdit(id) {
        Modal.create('Edit Role', 'modal-lg')

        var url = '{{ route('subscription.edit', ':id') }}'
        url = url.replace(':id', id)
        $.get(`${url}`, function(data) {
            Modal.html(data)

            $(".btn-simpan").on('click', function(e) {
                e.preventDefault()

                var form = $('#form')

                $("button").attr('disabled', true)

                var url = '{{ route('subscription.update', ':id') }}'
                    url = url.replace(':id', id)

                $.ajax({
                    url: `${url}`,
                    type: 'post',
                    dataType: 'json',
                    data: form.serializeArray(),
                    success: function(result) {
                        if (result.status == true) {
                            show_success(result.response)
                            setTimeout(() => {
                                window.location.reload()
                            }, 1000)
                        } else {
                            show_failed(result.response)
                            $("button").removeAttr('disabled')
                        }
                    },
                    error: function(result) {
                        $("button").removeAttr('disabled')
                        show_errors(result.responseJSON)
                    }
                })
            })
        })
    }

    function btnDelete(id) {
        var url = '{{ route('subscription.destroy', ':id') }}'
        url = url.replace(':id', id)
        Swal.fire({
            title: 'Confirmation',
            text: "Are you sure delete this data?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: `${url}`,
                    type: 'post',
                    dataType: 'json',
                    data: { _method: 'DELETE' },
                    success: function(result) {
                        if (result.status == true) {
                            show_success(result.response)
                            setTimeout(() => {
                                window.location.reload()
                            }, 1000)
                        } else {
                            show_failed(result.response)
                        }
                    },
                    error: function(result) {
                        show_errors(result.responseJSON)
                    }
                })
            } else {
                return false
            }
        })
    }
</script>
@endpush
