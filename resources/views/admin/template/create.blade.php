{!! eform_open('form') !!}
<div class="form-group">
    {!! eform_input('text', 'Name', 'name') !!}
</div>

<div class="form-group">
    {!! eform_input('text', 'Price', 'price') !!}
</div>

<div class="form-group">
    {!! eform_area('Description', 'description') !!}
</div>

{!! eform_close() !!}