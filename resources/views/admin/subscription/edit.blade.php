{!! eform_open('form') !!}
@method('PUT')
<div class="form-group">
    {!! eform_input('text', 'Name', 'name', $subscription->name) !!}
</div>

<div class="form-group">
    {!! eform_input('text', 'Price', 'price', number_format($subscription->price,0,',','.')) !!}
</div>

<div class="form-group">
    {!! eform_area('Description', 'description', $subscription->description) !!}
</div>

{!! eform_close() !!}