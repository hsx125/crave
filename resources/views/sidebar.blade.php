<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="">
            <img style="width: 70px" src="{{ asset('assets/logo.png') }}">
        </a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="">
            <img style="width: 40px" src="{{ asset('assets/logo.png') }}">
        </a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">MENU NAVIGATION</li>
        @foreach(sidebar() as $key => $value)
            @isset($value['access'])
                @if (in_array(auth()->user()->role, $value['access']))
                    @if(isset($value['sub']))
                        <li class="nav-item dropdown">
                            <a href="javascript:;" class="nav-link has-dropdown" data-toggle="dropdown">
                                <i class="{{ $value['icon'] }}"></i>
                                <span>{{ $value['label'] }}</span>
                            </a>
                            <ul class="dropdown-menu">
                                @foreach($value['sub'] as $k => $v)
                                    @isset($v['access'])
                                        @if (in_array(auth()->user()->role, $v['access']))
                                            <li class="link-sub {{ ( in_array(explode('.', $v['url'])[0], explode('.', request()->route()->getName())) ) ? 'active' : '' }}">
                                                <a class="nav-link" href="{{ ((Route::has($v['url'])) ? route($v['url']) : '') }}">{{ $v['label'] }}</a>
                                            </li>
                                        @endif
                                    @endisset
                                @endforeach
                            </ul>
                        </li>
                    @else
                    <li class="{{ ( in_array(explode('.', $key)[0], explode('.', request()->route()->getName())) ) ? 'active' : '' }}">
                        <a href="{{ ((Route::has($key)) ? route($key) : '') }}" class="nav-link"><i class="{{ $value['icon'] }}"></i><span>{{ $value['label'] }}</span></a>
                    </li>
                    @endif
                @endif
            @endisset
        @endforeach
    </ul>
</aside>
@push('js')
<script>

    $('li.link-sub').each(function() {
        if ($(this).hasClass('active')) {
            $(this).parent().css({
                'display':'block'
            })
            $(this).parent().parent().addClass('active')
        }
    })
</script>
@endpush
