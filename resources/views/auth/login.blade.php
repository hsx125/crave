<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ __('Login') }} - {{ config('app.name') }}</title>
    <link rel="stylesheet" href="{{ asset('assets/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/font-awesome/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css?v=' . microtime(true)) }}">
    <link rel="icon" href="{{ asset('assets/logo.png') }}">
</head>

<body>
    <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
            <div class="col-lg-4 mt-5">
                <div class="login-brand">
                    <img src="{{ asset('assets/logo.png') }}" alt="logo" style="width: 150px;" class="">
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4>Login</h4>
                    </div>
                    <div class="card-body">
                        @if(session()->has('success'))
                            <div class="alert alert-success">
                                {{ session('success') }}
                            </div>
                        @endif
                        @error('error')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                        <form method="POST" action="{{ route('store.login') }}">
                            @csrf
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Email" value="{{ old('email') }}" tabindex="1" required autofocus>
                                @error('email')
                                    <div class="invalid-feedback" style="display: block;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="d-block">
                                    <label for="password" class="control-label">Password</label>
                                </div>
                                <div class="input-group mb-3">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="Password" tabindex="2" required>
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" style="border-top-right-radius: 0.25rem;border-bottom-right-radius: 0.25rem;">
                                            <i class="fa fa-eye password-hide" style="cursor: pointer;" onclick="btnShowPassword()"></i>
                                            <i class="fa fa-eye-slash password-show" style="display: none; cursor:pointer" onclick="btnHidePassword()"></i>
                                        </div>
                                    </div>
                                </div>
                                @error('password')
                                    <div class="invalid-feedback" style="display: block;">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
                                    <label class="custom-control-label" for="remember-me">Ingat saya</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                    Login
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="text-center my-1">
                    &copy; <a href="">{{ config('app.name') }} {{ date('Y')}}</a>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/js/popper.min.js') }} "></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/js/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/js/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script>

        const btnShowPassword = () => {
            $('input#password').attr('type', 'text')
            $('.password-hide').hide()
            $('.password-show').show()
        }

        const btnHidePassword = () => {
            $('input#password').attr('type', 'password')
            $('.password-hide').show()
            $('.password-show').hide()
        }

        $('form').on('submit', function() {
            $('button').attr('disabled', true)
        })
        setTimeout(() => {
            $('div.alert').hide('slow')
        }, 5000)
    </script>
</body>

</html>
