<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    use HasFactory;

    protected $fillable = ['name','description','file','image'];

    public function subscriptions()
    {
        return $this->hasMany(Templatesubscription::class, 'id_template', 'id');
    }
}
