<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Templatesubscription extends Model
{
    use HasFactory;

    protected $table = 'template_subscriptions';

    protected $fillable = ['id_template', 'id_subscription'];

    public $timestamps = false;

    protected $with = ['subscription'];

    public function template()
    {
        return $this->belongsTo(Template::class, 'id', 'id_template');
    }

    public function subscription()
    {
        return $this->hasOne(Subscription::class, 'id', 'id_subscription');
    }
}
