<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'List User';
        return view('admin.user.index', compact('title'));
    }

    public function list(Request $request)
    {
        $data = [];

        $no = intval($request['start']);
        $start = intval($request['start']);
        $length = intval($request['length']);
        $filter = "";

        if ( isset($request['start']) && $request['length'] != -1 ) {

            $start = intval($request['start']);
            $length = intval($request['length']);
        }

        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $filter = trim($request['search']['value']);
        }

        $users = User::where('role', '=', 'admin')->orderBy('id','asc')
                ->skip($start)
                ->take($length)
                ->get();
                
        $total_record_filter = User::where('role', '=', 'admin')->count();
                                
        $data = [];

        foreach ($users as $value) {
            ++$no;
            $value->no = $no;
            $data[] = $value;    
        }

        return response()->json([
            "draw"            => isset ( $request['draw'] ) ? intval( $request['draw'] ) : 0,
            "recordsTotal"    => $total_record_filter,
            "recordsFiltered" => $total_record_filter,
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Create New User';
        return view('admin.user.create', compact('title'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|email|unique:users,email',
            'password'              => 'required|confirmed',
            'password_confirmation' => 'required'
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->email_verified_at = Carbon::now();
        $user->role = 'admin';
        if ($user->save()) {
            return redirect()->route('user.index')->with(['success' => 'Add user success.']);
        } else { 
            return redirect()->route('user.create')->withInput()->withErrors(['failed' => 'Something wrong. Try again later.']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $title = 'Edit User';
        $user = User::findOrFail($id);
        return view('admin.user.edit', compact('user', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = 'Edit User';
        $user = User::findOrFail($id);
        return view('admin.user.edit', compact('user', 'title'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'  => 'required',
            'email' => 'required|email|unique:users,email,' . $id . '',
            'password'              => 'confirmed',
            'password_confirmation' => ''
        ]);

        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = empty($request->password) ? $user->password : bcrypt($request->password);
        $user->role = 'admin';
        if ($user->save()) {
            return redirect()->route('user.index')->with(['success' => 'Update user success.']);
        } else { 
            return redirect()->route('user.create')->withInput()->withErrors(['failed' => 'Something wrong. Try again later.']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        if ($user->delete()) {
            return response()->json(['status' => true, 'response' => 'Delete user success.'], 200);
        } else {
            return response()->json(['status' => false, 'response' => 'Something wrong. Try again later.'], 422);
        }
    }
}
