<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'List Member';
        return view('admin.member.index', compact('title'));
    }

    public function list(Request $request)
    {
        $data = [];

        $no = intval($request['start']);
        $start = intval($request['start']);
        $length = intval($request['length']);
        $filter = "";

        if ( isset($request['start']) && $request['length'] != -1 ) {

            $start = intval($request['start']);
            $length = intval($request['length']);
        }

        if ( isset($request['search']) && $request['search']['value'] != '' ) {
            $filter = trim($request['search']['value']);
        }

        $users = User::where('role', '=', 'user')->orderBy('id','asc')
                ->skip($start)
                ->take($length)
                ->get();
                
        $total_record_filter = User::where('role', '=', 'user')->count();
                                
        $data = [];

        foreach ($users as $value) {
            ++$no;
            $value->no = $no;
            $data[] = $value;    
        }

        return response()->json([
            "draw"            => isset ( $request['draw'] ) ? intval( $request['draw'] ) : 0,
            "recordsTotal"    => $total_record_filter,
            "recordsFiltered" => $total_record_filter,
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::where('role', '=', 'user')->findOrFail($id);
        if($user->delete()) {
            return response()->json([
                'status'    => true,
                'response'  => 'Member berhasil dihapus.'
            ]);
        } else {
            return response()->json([
                'status'    => false,
                'response'  => 'Member gagal dihapus.'
            ]);
        }
    }
}
