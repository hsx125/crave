<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Subscription Plan';
        $subscriptions = Subscription::orderBy('price', 'asc')->get();
        return view('admin.subscription.index', compact('title', 'subscriptions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subscription.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'          => 'required',
            'price'         => 'required',
            'description'   => 'nullable'
        ]);

        $subscription = new Subscription;
        $subscription->name = $request->name;
        $subscription->price = str_replace('.', '', $request->price);
        $subscription->description = $request->description;
        if($subscription->save()) {
            return response()->json([
                'status'    => true,
                'response'  => 'Data berhasil disimpan.'
            ]);
        } else {
            return response()->json([
                'status'    => false,
                'response'  => 'Data gagal disimpan.'
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function show(Subscription $subscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function edit(Subscription $subscription)
    {
        return view('admin.subscription.edit', compact('subscription'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Subscription $subscription)
    {
        $request->validate([
            'name'          => 'required',
            'price'         => 'required',
            'description'   => 'nullable'
        ]);

        $subscription->name = $request->name;
        $subscription->price = str_replace('.', '', $request->price);
        $subscription->description = $request->description;
        if($subscription->save()) {
            return response()->json([
                'status'    => true,
                'response'  => 'Data berhasil disimpan.'
            ]);
        } else {
            return response()->json([
                'status'    => false,
                'response'  => 'Data gagal disimpan.'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Subscription  $subscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subscription $subscription)
    {
        if($subscription->delete()) {
            return response()->json([
                'status'    => true,
                'response'  => 'Data berhasil dihapus.'
            ]);
        } else {
            return response()->json([
                'status'    => false,
                'response'  => 'Data gagal dihapus.'
            ]);
        }
    }
}
