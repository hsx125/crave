<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        if (Auth::check() || Auth::viaRemember()) {
            return redirect()->intended('dashboard');
        }
        return view('auth.login');
    }


    public function login(Request $request)
    {
        $request->validate([
            'email'      => 'required',
            'password'      => 'required'
        ]);
        
        $remember = $request->has('remember') ? true : false;

        $data = [
            'email'         => $request->email,
            'password'      => $request->password,
        ];

        if (Auth::attempt($data, $remember)) {
            $request->session()->regenerate();
            if(auth()->user()->role == 'admin') {
                return redirect()->intended('admin/dashboard');
            } else {
                return redirect()->intended('member/dashboard');
            }
        } 
        
        return redirect()->route('login')->withInput()->withErrors(['error' => 'Credentials not found!']);
    }

    public function register()
    {
        return view('auth.register');
    }

    public function store_register(Request $request)
    {
        $request->validate([
            'name'                  => 'required',
            'email'                 => 'required|unique:users,email',
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role = 'user';
        $user->email_verified_at = Carbon::now();
        $save = $user->save();
        if ($save) {
            return redirect()->route('login')->with(['success' => 'Register account success.']);
        } else { 
            return redirect()->route('register')->withInput()->withErrors(['error' => 'Something wrong. Try again later.']);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
