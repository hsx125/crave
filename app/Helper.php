<?php

/**
 * Name : Custom Helper
 * Author : Ertha Dwi Setiyawan
 */

use App\Models\User;
use App\Services\HomeService;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

if(!function_exists('setting')) {
	function setting($item = '') {
		if(!empty($item)) {
			$setting = DB::table('settings')->where('varname', $item)->first();
			return $setting->varvalue ?? false;
		} else {
			$setting = DB::table('settings')->get();
			return $setting;
		}
	}
}


if(!function_exists('show_photo_user')) {
	function show_photo_user($user = 0) {
		$user = User::find($user);

        $photo = asset('assets/img/avatar/avatar-1.png');
        if(!empty($user->photo)) {
            if(Storage::exists(config('app.photo_path') . DIRECTORY_SEPARATOR . $user->photo)) {
                $photo = asset('storage/photo/files/' . $user->photo . '?v=' . sha1(microtime(true)));
            }
        }
		return $photo;
	}
}


if (!function_exists('sidebar')) {
	function sidebar() {
		$menu = [
			'dashboard.index'	=> [
				'label'	=> 'Dashboard',
				'icon'	=> 'fa fa-pie-chart',
				'access'	=> ['admin','user'],
			],
			'subscription.index'	=> [
				'label'	=> 'Subscription',
				'icon'	=> 'fa fa-rocket',
				'access'	=> ['admin'],
			],
			'template.index'	=> [
				'label'	=> 'Template Event',
				'icon'	=> 'fa fa-files-o',
				'access'	=> ['admin'],
			],

			'users.index'		=> [
				'label'	=> 'User Management',
				'icon'	=> 'fa fa-users',
				'access'	=> ['admin'],
				'sub'	=> [
					[
						'label'	=> 'Member',
						'url'	=> 'member.index',
						'access'	=> ['admin']
					],
					[
						'label'	=> 'User',
						'url'	=> 'user.index',
						'access'	=> ['admin']
					],
				]
			],

			'logout'	=> [
				'label'	=> 'Logout',
				'icon'	=> 'fa fa-sign-out',
				'access'	=> ['admin','user'],
			]
		];

		return $menu;
	}
}


if (!function_exists('eform_open')) {
	function eform_open($id) {
		return "<form id='".$id."' onsubmit='return false'>";
	}
}

if (!function_exists('eform_close')) {
	function eform_close() {
		return "</form>";
	}
}

if (!function_exists('eform_button')) {
	function eform_button($label, $name = 'btn-simpan', $type='Close') {
		$html = '
				<div style="margin-bottom:0;">
				        <button type="reset" class="btn btn-danger closed" data-dismiss="modal">
				            '.$type.'
				        </button>
				        <button class="btn btn-primary '.$name.' xcx" data-name="'.ucfirst($label).'" name="'.$name.'">
				            '.$label.' 
				        </button>
				</div>';

		return $html;
	}
}

if (!function_exists('eform_hidden')) {
	function eform_hidden($name = '',$value = '',$attribute = 'required') {
		$html = '<input type="hidden" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$attribute.'>';
		return $html;
	}
}

if (!function_exists('eform_input')) {
	function eform_input($type = 'text',$label = '',$name = '',$value = '',$attribute = 'required',$msg = '') {

		$required = '';

		if(!empty($msg)) $msg = '' . $msg . '';

		if(strpos($attribute, 'required') == true) $required = '<small style="color:red">(*)</small>';
		if($attribute == 'required') $required = '<small style="color:red">(*)</small>';


		$is_value = '';
		if(!empty($value)) $is_value = 'value="'.$value.'"';

		$html = "";

		if ($type == 'hidden') {
			
			$html = '<input type="'.$type.'" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$attribute.'>';

		}else{
			
			$html = '<div>
						<label for="'.$name.'">'.$label.' ' . $required . '</label>
				        <div>
				        	<input type="'.$type.'" placeholder="'.$label.'" class="form-control" id="'.$name.'" name="'.$name.'" '. $is_value . $attribute.'>
				        	'.$msg.'
				        </div>
				    </div>';

		}
		return $html;
	}
}

if (!function_exists('eform_file')) {
	function eform_file($label = '',$name = '', $value = '',$attribute = 'required', $msg = "") {

		$required = '';

		if(!empty($msg)) $msg = '' . $msg . '';

		if(!empty($attribute)) $required = '<small style="color:red">(*)</small>';

		$html = '<div>
					<label for="'.$name.'">'.$label.' ' . $required . '</label>
			        <div>
			        	<input type="file" placeholder="'.$label.'" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$attribute.'>
				        '.$msg.'
			        </div>
			    </div>';

		return $html;

	}
}

if (!function_exists('eform_area')) {
	function eform_area($label = '',$name = '', $value = '', $attribute = 'required', $msg = '') {

		$required = '';

		if(!empty($msg)) $msg = '' . $msg . '';

		if(!empty($attribute)) $required = '<small style="color:red">(*)</small>';

		$html = '<div>
					<label for="'.$name.'">'.$label.' ' . $required . '</label>
			        <div>
			        	<textarea rows="5" placeholder="'.$label.'" class="form-control" id="'.$name.'" name="'.$name.'" '.$attribute.'>'.$value.'</textarea>
			        	'.$msg.'
			        </div>
			    </div>';

		return $html;

	}
}


if (!function_exists('eform_select')) {
	function eform_select($label, $name, array $values = null, $selected = null, $attribute = 'required', $msg = '') {

		$select = "";

		if(!empty($msg)) $msg = '' . $msg . '';

		$required = '';

		if(!empty($attribute)) $required = '<small style="color:red">(*)</small>';

		if (!empty($values)) {
			
			$select .= "<option value=''>Pilih ".$label."</option>";

			foreach ($values as $key => $value) {
				
				$var = ($key == $selected) ? 'selected' : false;

				$select .= "<option value='".$key."' ".$var.">".$value."</option>";

			}

		}else{

			$select .= "<option value=''>Data ".$label." Kosong</option>";

		}

		$html = '<div>
				    <label for="'.$name.'">'.$label.' ' . $required . '</label>
				    <div>
				        <select name="'.$name.'" class="form-control" id="'.$name.'" '.$attribute.'>
				            '.$select.'
				        </select>
				        '.$msg.'
				    </div>
				</div>';
		return $html;
	}
}

if (!function_exists('eform_select_all')) {
	function eform_select_all($label, $name, array $values = null, $selected = null, $attribute = 'required', $msg = '') {

		$select = "";

		if(!empty($msg)) $msg = '' . $msg . '';

		$required = '';

		if(!empty($attribute)) $required = '<small style="color:red">(*)</small>';

		if (!empty($values)) {
			
			$select .= "<option value=''>Semua ".$label."</option>";

			foreach ($values as $key => $value) {
				
				$var = ($key == $selected) ? 'selected' : false;

				$select .= "<option value='".$key."' ".$var.">".$value."</option>";

			}

		}else{

			$select .= "<option value=''>Semua ".$label."</option>";

		}

		$html = '<div>
				    <label for="'.$name.'">'.$label.' ' . $required . '</label>
				    <div>
				        <select name="'.$name.'" class="form-control" id="'.$name.'" '.$attribute.'>
				            '.$select.'
				        </select>
				        '.$msg.'
				    </div>
				</div>';
		return $html;
	}
}

if (!function_exists('eform_input_inline')) {
	function eform_input_inline($type = 'text',$label = '',$name = '',$value = '',$attribute = 'required',$msg = '') {

		$required = '';

		if(!empty($msg)) $msg = '' . $msg . '';

		if(strpos($attribute, 'required') == true) $required = '<small style="color:red">(*)</small>';
		if($attribute == 'required') $required = '<small style="color:red">(*)</small>';


		$is_value = '';
		if(!empty($value)) $is_value = 'value="'.$value.'"';

		$html = "";

		if ($type == 'hidden') {
			
			$html = '<input type="'.$type.'" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$attribute.'>';

		}else{
			
			$html = '
						<label class="col-lg-2" for="'.$name.'">'.$label.' ' . $required . '</label>
				        <div class="col-lg-10">
				        	<input type="'.$type.'" placeholder="'.$label.'" class="form-control" id="'.$name.'" name="'.$name.'" '.$is_value . $attribute.'>
				        	'.$msg.'
				        </div>
				    ';

		}
		return $html;
	}
}

if (!function_exists('eform_file_inline')) {
	function eform_file_inline($label = '',$name = '', $value = '',$attribute = 'required', $msg = "") {

		$required = '';

		if(!empty($msg)) $msg = '' . $msg . '';

		if(!empty($attribute)) $required = '<small style="color:red">(*)</small>';

		$html = '
					<label class="col-lg-2" for="'.$name.'">'.$label.' ' . $required . '</label>
			        <div class="col-lg-10">
			        	<input type="file" placeholder="'.$label.'" class="form-control" id="'.$name.'" name="'.$name.'" value="'.$value.'" '.$attribute.'>
				        '.$msg.'
			        </div>
			    ';

		return $html;

	}
}

if (!function_exists('eform_area_inline')) {
	function eform_area_inline($label = '',$name = '', $value = '', $attribute = 'required', $msg = '') {

		$required = '';

		if(!empty($msg)) $msg = '' . $msg . '';

		if(!empty($attribute)) $required = '<small style="color:red">(*)</small>';

		$html = '
					<label class="col-lg-2" for="'.$name.'">'.$label.' ' . $required . '</label>
			        <div class="col-lg-10">
			        	<textarea rows="5" placeholder="'.$label.'" class="form-control" id="'.$name.'" name="'.$name.'" '.$attribute.'>'.$value.'</textarea>
			        	'.$msg.'
			        </div>
			    ';

		return $html;

	}
}


if (!function_exists('eform_select_inline')) {
	function eform_select_inline($label, $name, array $values = null, $selected = null, $attribute = 'required', $msg = '') {

		$select = "";

		if(!empty($msg)) $msg = '' . $msg . '';

		$required = '';

		if(!empty($attribute)) $required = '<small style="color:red">(*)</small>';

		if (!empty($values)) {
			
			$select .= "<option value=''>Pilih ".$label."</option>";

			foreach ($values as $key => $value) {
				
				$var = ($key == $selected) ? 'selected' : false;

				$select .= "<option value='".$key."' ".$var.">".$value."</option>";

			}

		}else{

			$select .= "<option value=''>Data ".$label." Kosong</option>";

		}

		$html = '
				    <label class="col-lg-2" for="'.$name.'">'.$label.' ' . $required . '</label>
				    <div class="col-lg-10">
				        <select name="'.$name.'" class="form-control" id="'.$name.'" '.$attribute.'>
				            '.$select.'
				        </select>
				        '.$msg.'
				    </div>
				';
		return $html;
	}
}

if (!function_exists('check_server_online')) {
	function check_server_online($ip, $port = '') {
		if(!empty($port)) $port = ':' . $port;
		$url = $ip . $port;
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_exec($ch);
		$health = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if ($health) {
			$json = json_encode(['health' => $health, 'status' => '1']);
			return $json;
		} else {
			$json = json_encode(['health' => $health, 'status' => '0']);
			return $json;
		}
	}
}