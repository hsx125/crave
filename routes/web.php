<?php

use App\Http\Controllers\AuthController;

// Admin Route

use App\Http\Controllers\Admin\{
    DashboardController,
    MemberController,
    SubscriptionController,
    TemplateController,
    UserController
};

// End Admin Route

// Member Route

use App\Http\Controllers\Member\{
    DashboardController as MemberDashboardController
};

// End Member Route

use Illuminate\Support\Facades\Route;

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('login', [AuthController::class, 'login'])->name('store.login');

Route::get('register', [AuthController::class, 'register'])->name('register');
Route::post('register', [AuthController::class, 'store_register'])->name('store.register');


Route::middleware(['auth'])->group(function() {

    Route::prefix('admin')->group(function() {
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard.index');
        Route::resource('member', MemberController::class);
        Route::post('member/list', [MemberController::class, 'list'])->name('member.list');
        Route::resource('user', UserController::class);
        Route::post('user/list', [UserController::class, 'list'])->name('user.list');
        Route::resource('subscription', SubscriptionController::class);
        Route::resource('template', TemplateController::class);
    });

    Route::prefix('member')->group(function() {
        Route::get('/dashboard', [MemberDashboardController::class, 'index'])->name('dashboard.index');
    });
    
});

Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
