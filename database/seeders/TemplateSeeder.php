<?php

namespace Database\Seeders;

use App\Models\Template;
use Illuminate\Database\Seeder;

class TemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Template::create([
            'name'  => 'Wedding A',
            'description' => 'Wedding A',
        ]);

        Template::create([
            'name'  => 'Wedding B',
            'description' => 'Wedding B',
        ]);
    }
}
