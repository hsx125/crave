<?php

namespace Database\Seeders;

use App\Models\Subscription;
use App\Models\Template;
use App\Models\Templatesubscription;
use Illuminate\Database\Seeder;

class TemplatesubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach(Template::all() as $template) {
            Templatesubscription::create([
                'id_template'   => $template->id,
                'id_subscription'   => Subscription::inRandomOrder()->first()->id,
            ]);
        }
    }
}
