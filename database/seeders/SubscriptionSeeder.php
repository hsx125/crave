<?php

namespace Database\Seeders;

use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Subscription::create([
            'name'  => 'Package A',
            'price' => 50
        ]);

        Subscription::create([
            'name'  => 'Package B',
            'price' => 125
        ]);

        Subscription::create([
            'name'  => 'Package C',
            'price' => 150
        ]);
    }
}
